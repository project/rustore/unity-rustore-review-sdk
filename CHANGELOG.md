## История изменений

### Release 8.0.0
- Версия SDK Review 8.0.0.

### Release 7.0.0
- Версия SDK Review 7.0.0.

### Release 6.1.0
- Версия SDK Review 6.1.0.
- RuStoreSDK помещена в отдельную assembly.

### Release 6.0.0
- Версия SDK Review 6.+.
- Изменена структура репозитория.
- Добавлен проект с исходным кодом пакетов `.aar`.

### Release 2.0.0
- Версия SDK Review 2.+.

### Release 0.1.7
- Исправлены зависимости SDK.

### Release 0.1.6
- Версия SDK Review 0.1.6.
